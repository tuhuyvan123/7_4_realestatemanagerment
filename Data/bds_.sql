-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2021 at 05:16 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bds`
--

-- --------------------------------------------------------

--
-- Table structure for table `batdongsan`
--

CREATE TABLE `batdongsan` (
  `bds_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bds_loai` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `bds_mota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_dientich` float NOT NULL,
  `bds_gia` float NOT NULL,
  `bds_kieu` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `bds_state` bit(1) NOT NULL,
  `bds_ngaydang` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `batdongsan`
--

INSERT INTO `batdongsan` (`bds_id`, `user_id`, `bds_loai`, `bds_mota`, `bds_hinh`, `bds_diachi`, `bds_dientich`, `bds_gia`, `bds_kieu`, `bds_state`, `bds_ngaydang`) VALUES
(1, 1, 'N', 'Uy tín, chất lượng là hàng đầu. Nhà đẹp, mới xây dựng.', '?PNG\r\n\Z\r\n\0\0\0\r\nIHDR\0\0\0?\0\0\0?\0\0\0?1?\0\0\0	pHYs\0\0\0\0\0??\0\0\0gAMA\0\0??|?Q?\0\0\0 cHRM\0\0z%\0\0??\0\0??\0\0??\0\0u0\0\0?`\0\0:?\0\0o?_?F\0JIDATxڼ?g???u?????????V??]]?9??@\0??(A?%˴,?py<?l??Y??e?????,mѣ???Y?I??@?nt@??P9?N|?????so?@ ?ֺ???\r\n?????????-????{?', '45 Đoàn Thị Điểm, Quận 6, HCM', 214.5, 300000000, 'B', b'0', '2021-04-01'),
(2, 2, 'D', 'Bán đất nền dự án, giàu tiềm năng.', '', '23 Khu tái định cư, Trần Phú, Quận 12, HCM', 114.5, 100000000, 'T', b'0', '2021-04-02');

-- --------------------------------------------------------

--
-- Table structure for table `nguoidung`
--

CREATE TABLE `nguoidung` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_hoten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_gioitinh` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `user_dienthoai` int(11) NOT NULL,
  `user_diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_ngaysinh` date NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_baidang` int(11) NOT NULL,
  `user_thanhvien` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `user_role` bit(1) NOT NULL,
  `user_actived` bit(1) NOT NULL,
  `user_activate_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nguoidung`
--

INSERT INTO `nguoidung` (`user_id`, `user_username`, `user_password`, `user_hoten`, `user_hinh`, `user_gioitinh`, `user_dienthoai`, `user_diachi`, `user_ngaysinh`, `user_email`, `user_baidang`, `user_thanhvien`, `user_role`, `user_actived`, `user_activate_token`) VALUES
(1, 'vantoan123', '123456', 'Nguyễn Văn Toàn', '', 'Nam', 1938417317, 'Lê Văn Lương, Quận 7, HCM', '1995-05-25', 'vantoan123@gmail.com', 0, 'D', b'0', b'0', ''),
(2, 'lanngo44', '123456', 'Ngô Thị Lan', '', 'Nu', 1968231657, 'Lê Thị Riêng, Quận 10, HCM', '1997-10-15', 'ngolan44@gmail.com', 3, 'B', b'0', b'0', ''),
(3, 'cnpm', '123', 'Trần Minh Phương', '', 'Nam', 1393841745, '34 Lê Lai, Quận 1, HCM', '2000-04-16', 'minhphuongbigboss280@gmail.com', 0, '', b'1', b'0', '');

-- --------------------------------------------------------

--
-- Table structure for table `tintucbatdongsan`
--

CREATE TABLE `tintucbatdongsan` (
  `ttbds_id` int(11) NOT NULL,
  `ttbds_tieude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_noidung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_theloai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_ngaydang` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tintucbatdongsan`
--

INSERT INTO `tintucbatdongsan` (`ttbds_id`, `ttbds_tieude`, `ttbds_noidung`, `ttbds_hinh`, `ttbds_theloai`, `ttbds_ngaydang`) VALUES
(1, 'Dự án khu tái định cư tiềm năng', 'Trước ngưỡng cửa của sự khan hiếm...', '', 'Dự án', '2021-04-03'),
(2, 'Được và mất khi đầu tư bất động sản', 'Việc dự đoán rủi ro luôn là điều...', '', 'Phân tích', '2021-04-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batdongsan`
--
ALTER TABLE `batdongsan`
  ADD PRIMARY KEY (`bds_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `nguoidung`
--
ALTER TABLE `nguoidung`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tintucbatdongsan`
--
ALTER TABLE `tintucbatdongsan`
  ADD PRIMARY KEY (`ttbds_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batdongsan`
--
ALTER TABLE `batdongsan`
  MODIFY `bds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nguoidung`
--
ALTER TABLE `nguoidung`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tintucbatdongsan`
--
ALTER TABLE `tintucbatdongsan`
  MODIFY `ttbds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `batdongsan`
--
ALTER TABLE `batdongsan`
  ADD CONSTRAINT `batdongsan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `nguoidung` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
