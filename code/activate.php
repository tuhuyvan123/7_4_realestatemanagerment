<?php
  require_once('db.php');
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>Activation</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/reality-icon.css">
        <link rel="stylesheet" type="text/css" href="css/bootsnav.css">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
        <link rel="stylesheet" type="text/css" href="css/settings.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/range-Slider.min.css">
        <link rel="stylesheet" type="text/css" href="css/search.css">
        <link rel="icon" href="images/icon.png">
    </head>

    <body>

        <!-- Page Banner Start-->


        <!-- Login -->
        <section id="login" class="padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="profile-login">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                             
                            </ul>
                            <?php
                                $error = '';
                                $message= '';
                                if (isset($_GET['email']) && isset($_GET['token'])){
                                    $email = $_GET['email'];
                                    $token = $_GET['token'];

                                    if (filter_var($email, FILTER_VALIDATE_EMAIL ) === false ){
                                    $error = 'Invalid email address';
                                    }
                                    else if (strlen($token) != 32){
                                    $error = 'Invalid token format';
                                    }
                                    else{
                                    $result = activeAccount($email, $token);
                                    if ($result['code'] == 0){
                                        $message = 'Your account has been activated. Login now';
                                    }else{
                                        $error = $result['error'];
                                    }
                                    }
                                }
                                else{
                                $error = 'Invalid activation url';
                                }
                            ?>

                            <?php
                                if (!empty($error)){
                                ?>
                                   <div class="tab-content padding_half">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            <div class="agent-p-form">
                                                <div class="single-query form-group col-sm-12 " >
                                                    <h4 style="font-size:30px; color:white;">Account Activation</h4>
                                                </div>
                                                <p style="font-size:30px; color:red"><?= $error ?></p>
                                                
                                                <a class="btn btn-success px=20" href="login.php">Login</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }else{
                                ?>
                                    
                                    <div class="tab-content padding_half">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            <div class="agent-p-form">
                                                <div class="single-query form-group col-sm-12 " >
                                                    <h4 style="font-size:30px; color:white;">Account Activation</h4>
                                                </div>
                                                <p class="text-success" style="font-size:30px;">Congratulations! Your account has been activated.</p>
                                                
                                                <a class="btn btn-success px=15" href="login.php">Login</a>
                                            </div>
                                        </div>
                                    </div>

                                    
                                <?php
                                }
                            
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Login end -->



       
            
      




        <script src="js/jquery-2.1.4.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/bootsnav.js"></script>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/jquery.cubeportfolio.min.js"></script>
        <script src="js/range-Slider.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/selectbox-0.2.min.js"></script>
        <script src="js/zelect.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <script src="js/jquery.themepunch.tools.min.js"></script>
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/revolution.extension.layeranimation.min.js"></script>
        <script src="js/revolution.extension.navigation.min.js"></script>
        <script src="js/revolution.extension.parallax.min.js"></script>
        <script src="js/revolution.extension.slideanims.min.js"></script>
        <script src="js/revolution.extension.video.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/functions.js"></script>
    </body>

    </html>