-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 25, 2021 lúc 05:00 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bds`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `batdongsan`
--

CREATE TABLE `batdongsan` (
  `bds_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bds_loai` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `bds_vip` bit(1) NOT NULL,
  `bds_tieude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_mota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bds_dientich` float NOT NULL,
  `bds_gia` float NOT NULL,
  `bds_kieu` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `bds_state` bit(1) NOT NULL,
  `bds_ngaydang` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `batdongsan`
--

INSERT INTO `batdongsan` (`bds_id`, `user_id`, `bds_loai`, `bds_vip`, `bds_tieude`, `bds_mota`, `bds_hinh`, `bds_diachi`, `bds_dientich`, `bds_gia`, `bds_kieu`, `bds_state`, `bds_ngaydang`) VALUES
(1, 1, 'N', b'0', 'Bán nhà mới xây dựng', 'Uy tín, chất lượng là hàng đầu. Nhà đẹp, mới xây dựng.', 'nhaphodanang.png', '45 Đoàn Thị Điểm, Quận 6, HCM', 214.5, 300000000, 'B', b'0', '2021-04-01'),
(2, 2, 'D', b'0', 'Đất nền cho thuê', 'Bán đất nền dự án, giàu tiềm năng.', 'bds_Xay-dung-cong-vien-phan-mem-lon-nhat-nuoc-37559-1.jpg', '23 Khu tái định cư, Trần Phú, Quận 12, HCM', 114.5, 100000000, 'T', b'1', '2021-04-02'),
(4, 19, 'C', b'0', 'Căn hộ chung cư cao cấp mới xây dựng', 'Là sự lựa chọn hoàn hảo cho người có nhu cầu về căn hộ chung cư', 'canhodanang.png', '45 Hoàng Hoa Thám, Quận 1, HCM', 45.5, 50000000, 'T', b'0', '2021-04-21'),
(5, 19, 'C', b'1', 'Căn hộ chung cư cao cấp quận 2', 'Căn hộ chung cư mới xậy dựng cao cấp, sự lựa chọn tuyệt vời cho người có thu nhập cao', 'efefed.png', 'Thảo Điền, Quận 2, HCM', 50.5, 150000000, 'T', b'0', '2021-04-21'),
(6, 2, 'D', b'1', 'Đất nền khu tái định cư', 'Bán đất nền dự án khu tái định cư', 'News_blu195.png', 'Khu tái định cư, Trần Xuân Soạn, Quận Bịnh Thạnh, HCM', 150.5, 1550000000, 'T', b'0', '2021-04-02'),
(7, 1, 'N', b'1', 'Bán nhà mới xây dựng, cao cấp', 'Nhà đẹp dọn vô liền, cao cấp, sang trọng', 'hjtyvinht.png', '23 Ngô Tất Tố, Quận Tân Bình, HCM', 230.5, 300000000, 'T', b'0', '2021-04-01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguoidung`
--

CREATE TABLE `nguoidung` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_hoten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_gioitinh` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `user_dienthoai` int(11) NOT NULL,
  `user_diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_ngaysinh` date NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_baidang` int(11) NOT NULL,
  `user_thanhvien` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `user_role` bit(1) NOT NULL,
  `user_actived` bit(1) NOT NULL,
  `user_activate_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nguoidung`
--

INSERT INTO `nguoidung` (`user_id`, `user_username`, `user_password`, `user_hoten`, `user_hinh`, `user_gioitinh`, `user_dienthoai`, `user_diachi`, `user_ngaysinh`, `user_email`, `user_baidang`, `user_thanhvien`, `user_role`, `user_actived`, `user_activate_token`) VALUES
(1, 'vantoan123', '123456', 'Nguyễn Văn Toàn', '', 'Nam', 1938417317, 'Lê Văn Lương, Quận 7, HCM', '1995-05-25', 'vantoan123@gmail.com', 0, 'D', b'0', b'0', ''),
(2, 'lanngo44', '123456', 'Ngô Thị Lan', '', 'Nu', 1968231657, 'Lê Thị Riêng, Quận 10, HCM', '1997-10-15', 'ngolan44@gmail.com', 3, 'B', b'0', b'0', ''),
(3, 'cnpm', '123', 'Trần Minh Phương', '', 'Nam', 1393841745, '34 Lê Lai, Quận 1, HCM', '2000-04-16', 'minhphuongbigboss@gmail.com', 0, '', b'1', b'0', ''),
(14, 'Tuhuyvan', '$2y$10$mPb/0lYRU7W2iwrv75gCEuy/mXNwmZaWatUIwPv0N3g9xpRvgq1ly', 'asdasdasd', '', 'Nam', 2147483647, 'asdasd', '0000-00-00', 'tuhuyvan123@gmail.com', 0, '', b'0', b'1', 'e4a5e4ea0eeb4cb56246026510214457'),
(19, 'tmp', '$2y$10$DRnWGL2RNRvcs0ZIXSxMU.WM6yxXngpYSuOT2opcjoBi2/lpUWauG', 'Trần Minh Phương', '', 'Nam', 379245526, '34 Phạm Văn Nghị, P.Tân Phong (Phúc Long Tea & Coffee - Sky Garden)', '0000-00-00', 'minhphuongbigboss280@gmail.com', 0, '', b'0', b'1', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintucbatdongsan`
--

CREATE TABLE `tintucbatdongsan` (
  `ttbds_id` int(11) NOT NULL,
  `ttbds_tieude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_noidung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_theloai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttbds_ngaydang` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintucbatdongsan`
--

INSERT INTO `tintucbatdongsan` (`ttbds_id`, `ttbds_tieude`, `ttbds_noidung`, `ttbds_hinh`, `ttbds_theloai`, `ttbds_ngaydang`) VALUES
(1, 'Dự án khu tái định cư tiềm năng', 'Trước ngưỡng cửa của sự khan hiếm...', 'fewrtevinh.png', 'Dự án', '2021-04-03'),
(2, 'Được và mất khi đầu tư bất động sản', 'Việc dự đoán rủi ro luôn là điều...', 'toanhachothue.jpg', 'Phân tích', '2021-04-04'),
(3, 'Giới thiệu tổng quan về Thành phố Biên Hòa Đồng Nai', 'Biên Hòa là một thành phố công nghiệp thuộc tỉnh Đồng Nai, được biết đến mức phát triển mạnh mẽ...', 'tqv_CAM UYEN-da hoa trang.JPG', 'Dự án', '2021-04-03'),
(4, 'Rót tiền đầu tư bất động sản, không phải ai cũng “trúng mánh”', 'Ôm mộng làm giàu từ bất động sản nhưng không phải nhà đầu tư nào tham gia thị trường...', 'tqv_thuthiem.jpg', 'Phân tích', '2021-04-03'),
(5, 'Giá bất động sản sẽ tăng hay giảm sau đợt bùng dịch mới?', 'Liệu giá bất động sản sẽ tăng tiếp hay giảm mạnh trong thời gian tới...', 'vanphongchothue.JPG', 'Phân tích', '2021-04-03');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `batdongsan`
--
ALTER TABLE `batdongsan`
  ADD PRIMARY KEY (`bds_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `nguoidung`
--
ALTER TABLE `nguoidung`
  ADD PRIMARY KEY (`user_id`);

--
-- Chỉ mục cho bảng `tintucbatdongsan`
--
ALTER TABLE `tintucbatdongsan`
  ADD PRIMARY KEY (`ttbds_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `batdongsan`
--
ALTER TABLE `batdongsan`
  MODIFY `bds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `nguoidung`
--
ALTER TABLE `nguoidung`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `tintucbatdongsan`
--
ALTER TABLE `tintucbatdongsan`
  MODIFY `ttbds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `batdongsan`
--
ALTER TABLE `batdongsan`
  ADD CONSTRAINT `batdongsan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `nguoidung` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
