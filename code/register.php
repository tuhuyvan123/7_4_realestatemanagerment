<?php
    session_start();

    
    require_once('db.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/reality-icon.css">
    <link rel="stylesheet" type="text/css" href="css/bootsnav.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="css/settings.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/range-Slider.min.css">
    <link rel="stylesheet" type="text/css" href="css/search.css">
    <link rel="icon" href="images/icon.png">
</head>

<body>
    <?php
        $error = '';
        $first_name = '';
        $phone = '';
        $email = '';
        $user = '';
        $pass = '';
        $pass_confirm = '';
        $address = '';
        $gender = '';
        if (isset($_POST['first']) && isset($_POST['email'])
        && isset($_POST['user']) && isset($_POST['pass']) && isset($_POST['pass-confirm']) && isset($_POST['phone']) && isset($_POST['address']) && isset($_POST['gender']) )
        {
            $first_name = $_POST['first'];
            $email = $_POST['email'];
            $user = $_POST['user'];
            $pass = $_POST['pass'];
            $pass_confirm = $_POST['pass-confirm'];
            $phone = $_POST['phone'];
            $address = $_POST['address'];
            $gender = $_POST['gender'];

            if (empty($user)) {
                $error = 'Please enter your username';
            }
            else if (empty($email)) {
                $error = 'Please enter your email';
            }
            else if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                $error = 'This is not a valid email address';
            }
            else if (empty($pass)) {
                $error = 'Please enter your password';
            }
            else if (strlen($pass) < 6) {
                $error = 'Password must have at least 6 characters';
            }
            else if(empty($pass_confirm)){
                $error = 'Please confirm your password';
            }
            else if ($pass != $pass_confirm) {
                $error = 'Password does not match';
            }
            else if (empty($first_name)) {
                $error = 'Please enter your full name';
            }
            else if (empty($phone)) {
                $error = 'Please enter your phone number';
            }
            else if (empty($address)) {
                $error = 'Please enter your address';
            }
            else {
                $result = register($user, $pass, $first_name, $email,$phone,$address,$gender);

                if ($result['code'] == 0) {
                    
                     header('Location: login.php');
                     exit();
                }
                if ($result['code'] == 1) $error = 'This email address is already being used'; 
                else $error = 'An error occured. Please try again later';
            }
        }
        
    ?>




        <!-- Page Banner Start-->
        <section class="page-banner padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="text-uppercase">Register</h1>
                        <p>Create new account</p>
                        <ol class="breadcrumb text-center">
                            <li><a href="index.php">Home</a></li>
                            <li class="active">Register</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!-- Page Banner End -->


        <!-- Register -->
        <section id="login" class="padding">
            <div class="container">
         
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="profile-login">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Register</a></li>

                            </ul>
                            <!-- Tab panes -->
                           
                            <div class="agent-p-form">
                                <form method="post" action="" novalidate class="callus clearfix">
                                    
                                    <div class="single-query col-sm-12 form-group">
                                        <input value="<?= $user?>" name="user" required class="form-control" type="text" placeholder="Username" id="user">    
                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                        <input value="<?= $email?>" name="email" required class="form-control" type="email" placeholder="Email" id="email">
                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                        <input  value="<?= $pass?>" name="pass" required class="form-control" type="password" placeholder="Password" id="pass">
                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                        <input value="<?= $pass_confirm?>" name="pass-confirm" required class="form-control" type="password" placeholder="Confirm Password" id="pass2"> 
                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                        <input value="<?= $first_name?>" name="first" required class="form-control" type="text" placeholder="Full name" id="firstname">
                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                        <input value="<?= $phone?>" name="phone" required class="form-control" type="text" placeholder="Phone numer" id="phonenumber">
                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                        <input value="<?= $address?>" name="address" required class="form-control" type="text" placeholder="Address" id="address">

                                    </div>
                                    <div class="single-query col-sm-12 form-group">
                                            <input type="radio" name="gender" value="Nam" checked>Male          
                                            <input type="radio" name="gender" value="Nu">Female
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                        <div class="form-group">
                                            <p></p>
                                                <button type="submit" class="btn btn-success px-5 mt-3 mr-2">Register</button>
                                            <p></p>
                                        </div>
                                        <?php
                                            if (!empty($error)) {
                                                echo "<div class='alert alert-danger'>$error</div>";
                                            }
                                        ?>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </section>
        <!-- Register end -->



        <footer class="padding_top footer2">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="footer_panel bottom30">
                            <a href="javascript:void(0)" class="logo bottom30"><img src="images/logo-white.png" alt="logo"></a>
                            <p class="bottom15">Giấy ĐKKD số 0104630479 do Sở KHĐT TP Hà Nội cấp lần đầu ngày 02/06/2010</p>
                            <p class="bottom16">Giấy phép ICP số 2399/GP-STTTT do Sở TTTT Hà Nội cấp ngày 04/09/2014</p>
                            <p class="bottom17">Giấy phép GH ICP số 3832/GP-TTĐT do Sở TTTT Hà Nội cấp ngày 08/08/2019</p>
                            <p class="bottom18">Giấy phép SĐ, BS GP ICP số 3833/GP-TTĐT do Sở TTTT Hà Nội cấp ngày 08/08/2019</p>
                            <p class="bottom19">Giấy xác nhận số 1728/GXN-TTĐT do Sở TTTT Hà Nội cấp ngày 23/06/2020</p>

                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer_panel bottom30">
                            <h4 class="bottom30">Latest News</h4>
                            <div class="media">

                                <div class="media-body">
                                    <a href="#.">Chi nhánh Hà Nội</a>
                                    <span><i class="icon-phone"></i></span>
                                    <span><i class="icon-icons74"></i></span>
                                </div>
                            </div>
                            <div class="media">

                                <div class="media-body">
                                    <a href="#.">Chi nhánh Thành Phố Hồ Chí Minh</a>
                                    <span><i class="icon-phone"></i></span>
                                    <span><i class="icon-icons74"></i></span>
                                </div>
                            </div>
                            <div class="media">

                                <div class="media-body">
                                    <a href="#.">Chi nhánh Nha Trang</a>
                                    <span><i class="icon-phone"></i></span>
                                    <span><i class="icon-icons74"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="footer_panel bottom30">
                            <h4 class="bottom30">Liên hệ</h4>
                            <ul class="getin_touch">
                                <li><i class="icon-telephone114"></i>01 900 234 567 - 68</li>
                                <li><a href="javascript:void(0)"><i class="icon-icons142"></i>info@castle.com</a></li>
                                <li><a href="javascript:void(0)"><i class="icon-browser2"></i>www.castle.com</a></li>
                                <li><i class="icon-icons74"></i></li>

                            </ul>
                            <ul class="social_share">
                                <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook-1"></i></a></li>
                                <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter-1"></i></a></li>
                                <li><a href="javascript:void(0)" class="google"><i class="icon-google4"></i></a></li>
                                <li><a href="javascript:void(0)" class="linkden"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="javascript:void(0)" class="vimo"><i class="icon-vimeo3"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>




        <script src="js/jquery-2.1.4.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/bootsnav.js"></script>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/jquery.cubeportfolio.min.js"></script>
        <script src="js/range-Slider.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/selectbox-0.2.min.js"></script>
        <script src="js/zelect.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <script src="js/jquery.themepunch.tools.min.js"></script>
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/revolution.extension.layeranimation.min.js"></script>
        <script src="js/revolution.extension.navigation.min.js"></script>
        <script src="js/revolution.extension.parallax.min.js"></script>
        <script src="js/revolution.extension.slideanims.min.js"></script>
        <script src="js/revolution.extension.video.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/functions.js"></script>
</body>

</html>