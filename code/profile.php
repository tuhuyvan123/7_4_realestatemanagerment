<?php 
    session_start();
 
    if (!isset($_SESSION['user'])) {
        header('Location: login.php');
        exit();
    }
    // echo $_SESSION['user'];
    require_once('db.php');
    $result = get_profile($_SESSION['user']);
    $data =$result['data'];
    // echo gettype($data['user_ngaysinh']);
    // echo $data['user_ngaysinh'];
    // echo strtotime($data['user_ngaysinh']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>Castle | Profile</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/reality-icon.css">
    <link rel="stylesheet" type="text/css" href="css/bootsnav.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
    <link rel="stylesheet" type="text/css" href="css/settings.css">
    <link rel="stylesheet" type="text/css" href="css/range-Slider.min.css">
    <link rel="stylesheet" type="text/css" href="css/search.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="images/icon.png">
</head>

<body>
    
    <!--Loader-->
    <div class="loader">
        <div class="span">
            <div class="location_indicator"></div>
        </div>
    </div>
    <!--Loader-->


    <header class="layout_double">
        <div class="topbar dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <p>We are Best in Town With 40 years of Experience.</p>
                    </div>
                    <div class="col-md-7 text-right">
                        <ul class="breadcrumb_top text-right">
                            <li><a href="favorite_properties.html"><i class="icon-icons43"></i>Favorites</a></li>
                            <li><a href="submit_property.html"><i class="icon-icons215"></i>Submit Property</a></li>
                            <li><a href="my_properties.html"><i class="icon-icons215"></i>My Property</a></li>
                            <li><a href="profile.php"><i class="icon-icons230"></i>Profile</a></li>
                            <!-- <li><a href="login.html"><i class="icon-icons179"></i>Login / Register</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-upper dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="logo">
                            <a href="index8.html"><img alt="" src="images/logo-white.png" class="img-responsive"></a>
                        </div>
                    </div>
                    <!--Info Box-->
                    <div class="col-md-9 col-sm-12 right">
                        <div class="info-box first">
                            <div class="icons"><i class="icon-telephone114"></i></div>
                            <ul>
                                <li><strong>Phone Number</strong></li>
                                <li>+1 900 234 567 - 68</li>
                            </ul>
                        </div>
                        <div class="info-box">
                            <div class="icons"><i class="icon-icons74"></i></div>
                            <ul>
                                <li><strong>Manhattan Hall,</strong></li>
                                <li>Castle Melbourne, australia</li>
                            </ul>
                        </div>
                        <div class="info-box">
                            <div class="icons"><i class="icon-icons142"></i></div>
                            <ul>
                                <li><strong>Email Address</strong></li>
                                <li><a href="#.">info@castle.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default navbar-sticky bootsnav">
            <div class="container">
                <div class="attr-nav">
                    <ul class="social_share clearfix">
                        <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#." class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="google" href="#."><i class="icon-google4"></i></a></li>
                    </ul>
                </div>
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
        <i class="fa fa-bars"></i>
        </button>
                    <a class="navbar-brand sticky_logo" href="index8.html"><img src="images/logo.png" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav" data-in="fadeIn" data-out="fadeOut">
                        <li class="dropdown active">
                            <a href="#." class="dropdown-toggle" data-toggle="dropdown">Home </a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">Home Style 1</a></li>
                                <li><a href="index2.html">Home Style 2</a></li>
                                <li><a href="index3.html">Home Style 3</a></li>
                                <li><a href="index4.html">Home Style 4</a></li>
                                <li> <a href="index5.html">Home Style 5</a></li>
                                <li> <a href="index6.html">Home Style 6</a></li>
                                <li> <a href="index7.html">Home Style 7</a></li>
                                <li> <a href="index8.html">Home Style 8</a></li>
                                <li> <a href="index9.html">Home Style 9</a></li>
                                <li> <a href="fullscreen.html">Home Fullscreen<span>new</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown megamenu-fw">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Listing</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        <div class="col-menu col-md-3">
                                            <h5 class="title">PROPERTIES LIST</h5>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="listing1.html">Properties List</a></li>
                                                    <li><a href="index7.html">Single Property</a></li>
                                                    <li><a href="listing2.html">Search by City</a></li>
                                                    <li><a href="listing5.html">Search by Category</a></li>
                                                    <li><a href="listing3.html">Search by Type</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-9">
                                            <h5 class="title bottom20">PROPERTIES LIST</h5>
                                            <div class="row">
                                                <div id="nav_slider" class="owl-carousel">
                                                    <div class="item">
                                                        <div class="image bottom15">
                                                            <img src="images/nav-slider1.jpg" alt="Featured Property">
                                                            <span class="nav_tag yellow text-uppercase">for rent</span>
                                                        </div>
                                                        <h4><a href="property_detail1.html">Park Avenue Apartment</a></h4>
                                                        <p>Towson London, MR 21501</p>
                                                    </div>
                                                    <div class="item">
                                                        <div class="image bottom15">
                                                            <img src="images/nav-slider2.jpg" alt="Featured Property">
                                                            <span class="nav_tag yellow text-uppercase">for rent</span>
                                                        </div>
                                                        <h4><a href="property_detail2.html">Park Avenue Apartment</a></h4>
                                                        <p>Towson London, MR 21501</p>
                                                    </div>
                                                    <div class="item">
                                                        <div class="image bottom15">
                                                            <img src="images/nav-slider3.jpg" alt="Featured Property">
                                                            <span class="nav_tag yellow text-uppercase">for rent</span>
                                                        </div>
                                                        <h4><a href="property_detail3.html">Park Avenue Apartment</a></h4>
                                                        <p>Towson London, MR 21501</p>
                                                    </div>
                                                    <div class="item">
                                                        <div class="image bottom15">
                                                            <img src="images/nav-slider1.jpg" alt="Featured Property">
                                                            <span class="nav_tag yellow text-uppercase">for rent</span>
                                                        </div>
                                                        <h4><a href="property_detail1.html">Park Avenue Apartment</a></h4>
                                                        <p>Towson London, MR 21501</p>
                                                    </div>
                                                    <div class="item">
                                                        <div class="image bottom15">
                                                            <img src="images/nav-slider2.jpg" alt="Featured Property">
                                                            <span class="nav_tag yellow text-uppercase">for rent</span>
                                                        </div>
                                                        <h4><a href="property_detail2.html">Park Avenue Apartment</a></h4>
                                                        <p>Towson London, MR 21501</p>
                                                    </div>
                                                    <div class="item">
                                                        <div class="image bottom15">
                                                            <img src="images/nav-slider3.jpg" alt="Featured Property">
                                                            <span class="nav_tag yellow text-uppercase">for rent</span>
                                                        </div>
                                                        <h4><a href="property_detail3.html">Park Avenue Apartment</a></h4>
                                                        <p>Towson London, MR 21501</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown megamenu-fw">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Properties</a>
                            <ul class="dropdown-menu megamenu-content bg" role="menu">
                                <li>
                                    <div class="row">
                                        <div class="col-menu col-md-3">
                                            <h5 class="title">PROPERTY LISTINGS</h5>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="listing1.html">List Style 1</a></li>
                                                    <li><a href="listing2.html">List Style 2</a></li>
                                                    <li><a href="listing3.html">List Style 3</a></li>
                                                    <li><a href="listing4.html">List Style 4</a></li>
                                                    <li><a href="listing5.html">List Style 5</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h5 class="title">PROPERTY LISTINGS</h5>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="listing6.html">List Style 5</a></li>
                                                    <li><a href="listing7.html">List Style 6</a></li>
                                                    <li><a href="listing1.html">Search by City</a></li>
                                                    <li><a href="listing2.html">Search by Category</a></li>
                                                    <li><a href="listing3.html">Search by Type</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h5 class="title">PROPERTY DETAIL</h5>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="property_detail1.html">Property Detail 1</a></li>
                                                    <li><a href="property_detail2.html">Property Detail 2</a></li>
                                                    <li><a href="property_detail3.html">Property Detail 3</a></li>
                                                    <li><a href="index7.html">Single Property</a></li>
                                                    <li><a href="listing4.html">Search by Type</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h5 class="title">OTHER PAGES</h5>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="favorite_properties.html">Favorite Properties</a></li>
                                                    <li><a href="agent_profile.html">Agent Profile</a></li>
                                                    <li><a href="404.html">404 Error</a></li>
                                                    <li><a href="contact.html">Contact Us</a></li>
                                                    <li><a href="testimonial.html">Testimonials</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#." class="dropdown-toggle" data-toggle="dropdown">Features </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown">
                                    <a href="#." class="dropdown-toggle" data-toggle="dropdown">News</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="agent1.html">news Style1</a></li>
                                        <li><a href="agent2.html">news Style2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#." class="dropdown-toggle" data-toggle="dropdown">Property Agents</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="agent1.html">Agent Style1</a></li>
                                        <li><a href="agent2.html">Agent Style2</a></li>
                                        <li><a href="agent3.html">Agent Style3</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#." class="dropdown-toggle" data-toggle="dropdown">Agetn Profile Styles</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="agent_profile.html">Agent Profile 1</a></li>
                                        <li><a href="agent_profile2.html">Agent Profile 2</a></li>
                                    </ul>
                                </li>
                                <li><a href="testimonial.html">Testimonials</a></li>
                                <li><a href="faq.html">FAQ's</a></li>
                                <li><a href="favorite_properties.html">Favorite Properties</a></li>
                                <li><a href="404.html">404 Error</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="https://themeforest.net/item/castle-real-estate-template/18593260?ref=BrighThemes">Buy Now</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!--Header Ends-->



    <!-- Page Banner Start-->
    <section class="page-banner padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="text-uppercase">Profile</h1>
                    <p>Serving you since 1999. Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
                    <ol class="breadcrumb text-center">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li class="active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Banner End -->




    <!-- Profile Start -->
    <section id="agent-2-peperty" class="profile padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="f-p-links margin_bottom">
                        <li><a href="profile.html" class="active"><i class="icon-icons230"></i>Profile</a></li>
                        <li><a href="my_properties.html"><i class="icon-icons215"></i> My Properties</a></li>
                        <li><a href="submit_property.html"><i class="icon-icons215"></i> Submit Property</a></li>
                        <li><a href="favorite_properties.html"><i class="icon-icons43"></i> Favorite Properties</a></li>
                        <li><a href="logout.php"><i class="icon-lock-open3" ></i>Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-3">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h2 class="text-uppercase bottom30">my profile</h2>
                    <div class="agent-p-img">
                        <img src="images/nguoidung/<?=$data['user_hinh']?>" class="img-responsive" alt="image" />
                        <a href="#" class="top10 bottom20">Update Profile Picture</a>
                        <p class="text-center">----------</p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="profile-form">
                        <div class="row">
                            <!-- <?php
                                if (isset($_POST['submit'])){
                                
                                    $error = '';
                                    $hoten = '';
                                    $dienthoai = '';
                                    $gioitinh = '';
                                    $email = '';
                                    $ngaysinh = '';
                                    $about = '';
                                
                                    // if (isset($_POST['hoten'])
                                        // && isset($_POST['user']) && isset($_POST['pass']) && isset($_POST['pass-confirm']) && isset($_POST['phone']) && isset($_POST['address']) && isset($_POST['gender']) )
                                    // {
                                    $hoten = $_POST['hoten'];
                                    $dienthoai = $_POST['dienthoai'];
                                    $gioitinh = $_POST['gioitinh'];
                                    $email = $_POST['email'];
                                    $ngaysinh = $_POST['ngaysinh'];
                                    $about = $_POST['about'];

                                    if (empty($user)) {
                                        $error = 'Please enter your username';
                                    }
                                    else if (empty($email)) {
                                        $error = 'Please enter your email';
                                    }
                                    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                                        $error = 'This is not a valid email address';
                                    }
                                    else if (strlen($dienthoai) != 11) {
                                        $error = 'This is not a valid phone number';
                                    }
                                    else {
                                        $result = change_profile($hoten, $dienthoai, $gioitinh,$email,$ngaysinh,$about);
                                        echo "okddddddddddddddddddddddddddddddddddddddddddd";
                                    }

                                //     // if ($result['code'] == 0) {
                                //     //     header('Location: profile.php');
                                //     //     exit();
                                //     // }
                                //     if ($result['code'] == 0) $error = 'Profile Changed'; 
                                //     else $error = 'An error occured. Please try again later';
                                // }
                                }
                            ?>  -->
                            <form class="callus" action="profile.php" method="POST">
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Họ tên:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="hoten"  placeholder="<?=$data['user_hoten']?>" class="keyword-input">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Điện thoại:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="dienthoai" placeholder="<?=$data['user_dienthoai']?>" class="keyword-input">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Giới tính:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="gioitinh" placeholder="<?=$data['user_gioitinh']?>" class="keyword-input">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Email:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="email" placeholder="<?=$data['user_email']?>" class="keyword-input">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Ngày sinh:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="ngaysinh" placeholder="<?=$data['user_ngaysinh']?>" class="keyword-input">
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Số bài đăng:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="sobaidang" value="<?=$data['user_baidang']?>" class="keyword-input" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>Thành viên:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <input type="text" name="thanhvien" 
                                        <?php
                                        if ($data['user_thanhvien'] == 'D') {
                                            ?>
                                            value="Đồng"
                                        <?php 
                                        } else if($data['user_thanhvien'] == 'B') {
                                        ?>
                                            value="Bạc"
                                        <?php 
                                        } else if($data['user_thanhvien'] == 'V'){
                                        ?>
                                            value="Vàng"
                                        <?php 
                                        } 
                                        ?>
                                            class="keyword-input" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-query">
                                        <label>About:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="single-query form-group">
                                        <textarea name="about" placeholder="<?=$data['user_about']?>" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    <button name='submit' class="btn-blue border_radius">Save Changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        

        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-6 col-xs-12 profile-form margin40">
                    <h3 class="bottom30 margin40">My Social Network</h3>
                    <div class="row">
                        <form class="callus">
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>Facebook:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="text" placeholder="http://facebook.com" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>Twitter:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="text" placeholder="http://twitter.com" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>Google Plus:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="text" placeholder="http://google-plus.com" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>Linkedin:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="text" placeholder="http://linkedin.com" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                <a class="btn-blue border_radius" href="#.">Save Changes</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2 hidden-xs"></div>
                <div class="col-md-5 col-sm-6 col-xs-12 profile-form margin40">
                    <h3 class=" bottom30 margin40">Change Your Password</h3>
                    <div class="row">
                        <form class="callus">
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>Current Password:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="password" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>New Password:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="password" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-query">
                                    <label>Confirm Password:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="single-query form-group">
                                    <input type="password" class="keyword-input">
                                </div>
                            </div>
                            <div class="col-sm-12 text-right">
                                <a class="btn-blue border_radius" href="#.">Save Changes</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Profile end -->

    <!--Footer-->
    <footer class="padding_top footer2">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer_panel bottom30">
                        <a href="javascript:void(0)" class="logo bottom30"><img src="images/logo-white.png" alt="logo"></a>
                        <p class="bottom15">Giấy ĐKKD số 0104630479 do Sở KHĐT TP Hà Nội cấp lần đầu ngày 02/06/2010</p>
                        <p class="bottom16">Giấy phép ICP số 2399/GP-STTTT do Sở TTTT Hà Nội cấp ngày 04/09/2014</p>
                        <p class="bottom17">Giấy phép GH ICP số 3832/GP-TTĐT do Sở TTTT Hà Nội cấp ngày 08/08/2019</p>
                        <p class="bottom18">Giấy phép SĐ, BS GP ICP số 3833/GP-TTĐT do Sở TTTT Hà Nội cấp ngày 08/08/2019</p>
                        <p class="bottom19">Giấy xác nhận số 1728/GXN-TTĐT do Sở TTTT Hà Nội cấp ngày 23/06/2020</p>

                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="footer_panel bottom30">
                        <h4 class="bottom30">Latest News</h4>
                        <div class="media">

                            <div class="media-body">
                                <a href="#.">Chi nhánh Hà Nội</a>
                                <span><i class="icon-phone"></i></span>
                                <span><i class="icon-icons74"></i></span>
                            </div>
                        </div>
                        <div class="media">

                            <div class="media-body">
                                <a href="#.">Chi nhánh Thành Phố Hồ Chí Minh</a>
                                <span><i class="icon-phone"></i></span>
                                <span><i class="icon-icons74"></i></span>
                            </div>
                        </div>
                        <div class="media">

                            <div class="media-body">
                                <a href="#.">Chi nhánh Nha Trang</a>
                                <span><i class="icon-phone"></i></span>
                                <span><i class="icon-icons74"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer_panel bottom30">
                        <h4 class="bottom30">Liên hệ</h4>
                        <ul class="getin_touch">
                            <li><i class="icon-telephone114"></i>01 900 234 567 - 68</li>
                            <li><a href="javascript:void(0)"><i class="icon-icons142"></i>info@castle.com</a></li>
                            <li><a href="javascript:void(0)"><i class="icon-browser2"></i>www.castle.com</a></li>
                            <li><i class="icon-icons74"></i></li>

                        </ul>
                        <ul class="social_share">
                            <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook-1"></i></a></li>
                            <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter-1"></i></a></li>
                            <li><a href="javascript:void(0)" class="google"><i class="icon-google4"></i></a></li>
                            <li><a href="javascript:void(0)" class="linkden"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="javascript:void(0)" class="vimo"><i class="icon-vimeo3"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery-2.1.4.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/jquery-countTo.js"></script>
    <script src="js/bootsnav.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/jquery.cubeportfolio.min.js"></script>
    <script src="js/range-Slider.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/selectbox-0.2.min.js"></script>
    <script src="js/zelect.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.themepunch.tools.min.js"></script>
    <script src="js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/revolution.extension.layeranimation.min.js"></script>
    <script src="js/revolution.extension.navigation.min.js"></script>
    <script src="js/revolution.extension.parallax.min.js"></script>
    <script src="js/revolution.extension.slideanims.min.js"></script>
    <script src="js/revolution.extension.video.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/functions.js"></script>

</body>

</html>