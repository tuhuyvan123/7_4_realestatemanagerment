<?php
    session_start();
 
    if (isset($_SESSION['user'])) {
        header('Location: index.php');
        exit();
    }
  
    require_once('db.php');
    $error = '';

    $user = '';
    $pass = '';

    if (isset($_POST['user']) && isset($_POST['pass'])) {
        $user = $_POST['user'];
        $pass = $_POST['pass'];

        if (empty($user)) {
            $error = 'Please enter your username';
        }
        else if (empty($pass)) {
            $error = 'Please enter your password';
        }
        else if (strlen($pass) < 6) {
            $error = 'Password must have at least 6 characters';
        }
        else{
          
            $result = login($user, $pass);
            if ($result['code'] == 0){
                $data = $result['data'];
                $_SESSION['user'] = $user;
                // $_SESSION['name'] = $data['firstname'] .' '. $data['lastname'];
                

                header('Location: index.php');
                exit();
            }else {
                $error = $result['error'];
            }
        }
    }
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/reality-icon.css">
        <link rel="stylesheet" type="text/css" href="css/bootsnav.css">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
        <link rel="stylesheet" type="text/css" href="css/settings.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/range-Slider.min.css">
        <link rel="stylesheet" type="text/css" href="css/search.css">
        <link rel="icon" href="images/icon.png">
    </head>

    <body>

        <!-- Page Banner Start-->
        <section class="page-banner padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="text-uppercase">Login</h1>
                        <p></p>
                        <ol class="breadcrumb text-center">
                            <li><a href="listing.html">Home</a></li>
                            <li class="active">Login</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!-- Page Banner End -->


        <!-- Login -->
        <section id="login" class="padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="profile-login">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Login</a></li>
                             
                            </ul>
                            <form method="post" action="" >
                                <div class="tab-content padding_half">
                                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                                        <div class="agent-p-form">
                                            <form action="#" class="callus clearfix">
                                                <div class="single-query form-group col-sm-12">
                                                    <input value="<?= $user ?>" name="user" id="user" type="text" class="form-control" placeholder="Username">
                                                </div>
                                                <div class="single-query form-group  col-sm-12">
                                                    <input name="pass" value="<?= $pass ?>" id="password" type="password" class="form-control" placeholder="Password">
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="search-form-group white form-group text-left">
                                                                <div class="check-box-2"><i><input type="checkbox" name="check-box"></i></div>
                                                                <span>Remember Login</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right">
                                                            <a href="forgot.php" class="lost-pass">Lost your password?</a>
                                                        </div>
                                                  
                                                        <div class="col-sm-6 text-right" style="margin-top:5px">
                                                            <a href="register.php" class="lost-pass">Register new account</a>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                <div class=" col-sm-12">
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <?php
                                                        if (!empty($error)) {
                                                            echo "<div class='alert alert-danger'>$error</div>";
                                                            
                                                        }
                                                    ?>
                                                     <button type="submit" class="btn px-10">Login</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Login end -->



        <footer class="padding_top footer2">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="footer_panel bottom30">
                            <a href="javascript:void(0)" class="logo bottom30"><img src="images/logo-white.png" alt="logo"></a>
                            <p class="bottom15">Giấy ĐKKD số 0104630479 do Sở KHĐT TP Hà Nội cấp lần đầu ngày 02/06/2010</p>
                            <p class="bottom16">Giấy phép ICP số 2399/GP-STTTT do Sở TTTT Hà Nội cấp ngày 04/09/2014</p>
                            <p class="bottom17">Giấy phép GH ICP số 3832/GP-TTĐT do Sở TTTT Hà Nội cấp ngày 08/08/2019</p>
                            <p class="bottom18">Giấy phép SĐ, BS GP ICP số 3833/GP-TTĐT do Sở TTTT Hà Nội cấp ngày 08/08/2019</p>
                            <p class="bottom19">Giấy xác nhận số 1728/GXN-TTĐT do Sở TTTT Hà Nội cấp ngày 23/06/2020</p>

                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer_panel bottom30">
                            <h4 class="bottom30">Latest News</h4>
                            <div class="media">

                                <div class="media-body">
                                    <a href="#.">Chi nhánh Hà Nội</a>
                                    <span><i class="icon-phone"></i></span>
                                    <span><i class="icon-icons74"></i></span>
                                </div>
                            </div>
                            <div class="media">

                                <div class="media-body">
                                    <a href="#.">Chi nhánh Thành Phố Hồ Chí Minh</a>
                                    <span><i class="icon-phone"></i></span>
                                    <span><i class="icon-icons74"></i></span>
                                </div>
                            </div>
                            <div class="media">

                                <div class="media-body">
                                    <a href="#.">Chi nhánh Nha Trang</a>
                                    <span><i class="icon-phone"></i></span>
                                    <span><i class="icon-icons74"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="footer_panel bottom30">
                            <h4 class="bottom30">Liên hệ</h4>
                            <ul class="getin_touch">
                                <li><i class="icon-telephone114"></i>01 900 234 567 - 68</li>
                                <li><a href="javascript:void(0)"><i class="icon-icons142"></i>info@castle.com</a></li>
                                <li><a href="javascript:void(0)"><i class="icon-browser2"></i>www.castle.com</a></li>
                                <li><i class="icon-icons74"></i></li>

                            </ul>
                            <ul class="social_share">
                                <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook-1"></i></a></li>
                                <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter-1"></i></a></li>
                                <li><a href="javascript:void(0)" class="google"><i class="icon-google4"></i></a></li>
                                <li><a href="javascript:void(0)" class="linkden"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="javascript:void(0)" class="vimo"><i class="icon-vimeo3"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>




        <script src="js/jquery-2.1.4.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/bootsnav.js"></script>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/jquery.cubeportfolio.min.js"></script>
        <script src="js/range-Slider.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/selectbox-0.2.min.js"></script>
        <script src="js/zelect.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <script src="js/jquery.themepunch.tools.min.js"></script>
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/revolution.extension.layeranimation.min.js"></script>
        <script src="js/revolution.extension.navigation.min.js"></script>
        <script src="js/revolution.extension.parallax.min.js"></script>
        <script src="js/revolution.extension.slideanims.min.js"></script>
        <script src="js/revolution.extension.video.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/functions.js"></script>
    </body>

    </html>