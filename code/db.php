<?php
    //Import PHPMailer classes into the global namespace
    //These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    require 'vendor/autoload.php';
    define('HOST','localhost');
    define('USER','root');
    define('PASS',"");
    define('DB','bds');

    function open_database(){
        $conn =new mysqli(HOST, USER, PASS, DB);
        if($conn-> connect_error){
            die('Connect error: ' . $conn->connect_error);
        }
        return $conn;
    }

    function login($user, $pass){
        $sql = "select * from nguoidung where user_username = ?";
        $conn = open_database();

        $stm = $conn->prepare($sql);
        $stm->bind_param('s', $user);
        if(!$stm->execute()){
            return array('code' =>1, 'error'=> 'Can not execute command');
        }

        $result = $stm->get_result();

        if ($result->num_rows == 0){
            return array('code' =>1, 'error'=> 'user does not exist');
        }

        $data = $result->fetch_assoc();
        echo ($pass);
        // echo($data['user_password']);
        $hashed_password = $data['user_password'];
        if (!password_verify($pass, $hashed_password)){
            return array('code' =>2, 'error'=> 'Invalid password');
        }
        else if ($data['user_actived'] == 0){
            return array('code' =>3, 'error'=> 'This account is not activated');
        
        }
        return array('code' =>0, 'error'=> '','data'=>$data);
    }
    function get_profile($user){
        $sql = "select * from nguoidung where user_username = ?";
        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('s',$user);
        if(!$stm->execute()){
            return array('code' =>1, 'error'=> 'Can not execute command');
        }
        $result = $stm->get_result();

        if ($result->num_rows == 0){
            return array('code' =>1, 'error'=> 'user does not exist');
        }
        $data = $result->fetch_assoc();
        return array('code' =>0, 'error'=> '','data'=>$data);
    }

    function is_email_exists($email){
        $sql = 'select user_username from nguoidung where user_email = ?';
        $conn = open_database();

        $stm = $conn->prepare($sql);
        $stm->bind_param('s',$email);
        if (!$stm->execute()){
            die('Query error: ' . $stm->error);
        }
        $result = $stm->get_result();
        if ($result->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    function register($user, $pass, $first, $email,$phone,$address,$gender){
        if (is_email_exists($email)){
            return array('code' => 1, 'error' =>'Email exists');
        }
        $hash = password_hash($pass, PASSWORD_DEFAULT);
        $rand = random_int(0,1000);
        $token = md5($user .'+'. $rand);
        $sql = 'insert into nguoidung(user_username, user_password,user_hoten , user_gioitinh, user_dienthoai,user_diachi ,user_email,user_activate_token) value (?,?,?,?,?,?,?,?)' ;

        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('ssssisss', $user, $hash,$first,$gender,$phone,$address,$email,$token);
        if (!$stm->execute()){
            return array('code' => 2, 'error' =>'Can not execute command');
        }
        sendActivationEmail($email, $token);
        return array ('code'=> 0, 'error'=>'Create account successful');
    }

    function sendActivationEmail($email, $token){
            

            //Instantiation and passing `true` enables exceptions
            $mail = new PHPMailer(true);
            
            try {
                //Server settings
                //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                $mail->isSMTP();                                            //Send using SMTP
                $mail->CharSet = 'UTF-8';  
                $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                $mail->Username   = 'castlerealestatead@gmail.com';                     //SMTP username
                $mail->Password   = 'realestate123';                               //SMTP password
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                //Recipients
                $mail->setFrom('castlerealestatead@gmail.com', 'Admin');
                $mail->addAddress($email, 'Người nhận');     //Add a recipient
                /*$mail->addAddress('ellen@example.com');               //Name is optional
                $mail->addReplyTo('info@example.com', 'Information');
                $mail->addCC('cc@example.com');
                $mail->addBCC('bcc@example.com');*/

                //Attachments
                //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

                //Content
                $mail->isHTML(true);                                  //Set email format to HTML
                $mail->Subject = 'Xác minh tài khoản của bạn';
                $mail->Body    = "Click <a href='http://localhost:63342/FTCNPM/activate.php?email=$email&token=$token'>vào đây <a> để xác minh tài khoản";
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->send();
                return true;
            } catch (Exception $e) {
               return false;
            }
    }

    function send_reset_email($email, $token){
            

        //Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
            $mail->isSMTP();  
            $mail->CharSet = 'UTF-8';                                          //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = 'castlerealestatead@gmail.com';                     //SMTP username
            $mail->Password   = 'realestate123';                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom('castlerealestatead@gmail.com', 'Admin');
            $mail->addAddress($email, 'Người nhận');     //Add a recipient
            /*$mail->addAddress('ellen@example.com');               //Name is optional
            $mail->addReplyTo('info@example.com', 'Information');
            $mail->addCC('cc@example.com');
            $mail->addBCC('bcc@example.com');*/

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'Khôi phục tài khoản của bạn';
            $mail->Body    = "Click <a href='http://localhost:63342/FTCNPM/reset_password.php?email=$email&token=$token'>vào đây <a> để khôi phục mật khẩu của bạn";
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            return true;
        } catch (Exception $e) {
           return false;
        }
    }   

    function activeAccount($email, $token){
        $sql = 'select user_username from nguoidung where user_email = ? and user_activate_token = ? and user_actived = 0';
        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('ss', $email, $token);

        if (!$stm->execute()){
            return array('code'=> 1, 'error'=> 'Can not execute command');
        }
        $result = $stm->get_result();
        if ($result->num_rows == 0){
            return array('code'=> 2, 'error'=> 'Email address or token not found');
        }
        $sql = "update nguoidung set user_actived = 1, user_activate_token = '' where user_email = ?";
        $stm = $conn->prepare($sql);
        $stm->bind_param('s', $email);

        if (!$stm->execute()){
            return array('code'=> 1, 'error'=> 'Can not execute command');
        }
        return array('code'=> 0, 'message'=> 'Account activated');

    }

    function reset_password($email){
        if(!is_email_exists($email)){
            return array('code' => 1,'error'=>'Email does not exist');
        }
        $exp = time() + 3600 * 24;
        $token = md5($email . '+' . random_int(1000,2000)) ;
        $sql = 'update reset_token set token = ? and expire_on = ? where email = ?';

        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('ssi',$token,$email,$exp);
        if(!$stm->execute()){
            return array('code' => 2,'error'=>'Can not execute command 2');
        }
        if ($stm->affected_rows == 0){
            $exp = time() + 3600 * 24;
            $sql = 'insert into reset_token values(?,?,?)';
            $stm = $conn->prepare($sql);
            $stm->bind_param('ssi',$email, $token, $exp);
            if(!$stm->execute()){
                return array('code' => 1,'error'=>'Already exist reset password email');
            }
        }

        $success = send_reset_email($email, $token);
        return array('code' => 0,'success'=>$success);
    }

    function change_password($email,$pass){
        if(!is_email_exists($email)){
            return array('code' => 1,'error'=>'Email does not exist');
        }
        $hash = password_hash($pass, PASSWORD_DEFAULT);
        $result = get_old_password($email);
        if (!password_verify($hash, $result)){
            $sql = 'update account set password = ? where email = ?';
        
            $conn = open_database();
            $stm = $conn->prepare($sql);
            $stm->bind_param('ss',$hash,$email);
            if(!$stm->execute()){
                return array('code' => 2,'error'=> 'Can not execute command');
            }
            return array('code' => 0,'message'=> 'Password Changed!');
        }else{
            return array('code' => 2,'error'=> 'New password must be different from old passoword');
        } 
    }
    function change_profile($hoten, $dienthoai, $gioitinh, $email,$ngaysinh,$about){
        $conn = open_database();
        $sql = 'update nguoidung set $user_hoten = ? and $user_dienthoai = ? and
        $user_gioitinh = ? and $user_email = ? and $user_ngaysinh = ? and $user_about = ? where user = ?';

        $stm = $conn->prepare($sql);
        $stm->bind_param('sissss',$hoten, $dienthoai, $gioitinh, $email, $ngaysinh, $about);
        if(!$stm->execute()){
            return array('code' => 1,'error'=> 'Can not execute command');
        }
        return array('code' => 0,'success'=> 'Profile Changed!');
    }

    function get_old_password($email){
        $conn = open_database();
        $sql = "select * from account where email = ?";
        $stm = $conn->prepare($sql);
        $stm->bind_param('s',$email);
        
        $stm->execute();
        $result = $stm->get_result();
        $row = $result->fetch_assoc();
        $pass = $row['password'];
        return $pass;
       
    }

    function display_batdongsan(){
        $sql = "select * from batdongsan";
        $conn = open_database();

        $stm = $conn->prepare($sql);
        if(!$stm->execute()){
            return array('code' => 2,'error'=> 'Can not execute command');
        }
        $result = $stm->get_result();
        if ($result->num_rows == 0){
            return array('code'=> 1, 'error'=> 'No product');
        }
        $data = array();

        while($row = $result->fetch_assoc()){
            $data[] = $row;
        }

        return array('code' => 0,'message'=> 'success','data'=>$data);
    }
    function display_tintucbatdongsan(){
        $sql = "select * from tintucbatdongsan";
        $conn = open_database();

        $stm = $conn->prepare($sql);
        if(!$stm->execute()){
            return array('code' => 2,'error'=> 'Can not execute command');
        }
        $result = $stm->get_result();
        if ($result->num_rows == 0){
            return array('code'=> 1, 'error'=> 'No product');
        }
        $data = array();

        while($row = $result->fetch_assoc()){
            $data[] = $row;
        }

        return array('code' => 0,'message'=> 'success','data'=>$data);
    }
    function add_product($name,$price,$desc,$image){
        $sql = 'insert into product(name, price, description, image) value (?,?,?,?)' ;

        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('siss',$name,$price,$desc,$image);
        if(!$stm->execute()){
            return array('code' => 2, 'error' => 'Can not execute command');
        }

        return array('code' => 0, 'message' => 'Add successful');
    }
    function edit_product($id,$name,$price,$desc,$image){
        $sql = "update product set name = ?, price = ?, description = ?, image = ? where id = ?;";

        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('sissi',$name,$price,$desc,$image,$id);
        if(!$stm->execute()){
            return array('code' => 2, 'error' => 'Can not execute command');
        }

        return array('code' => 0, 'message' => 'Add successful');
    }

    function delete_product($id){
        $sql = "Delete from product where id = ?";

        $conn = open_database();
        $stm = $conn->prepare($sql);
        $stm->bind_param('i',$id);
        if(!$stm->execute()){
            return array('code' => 2, 'error' => 'Can not execute command');
        }

        return array('code' => 0, 'message' => 'delete successful');
    }
    if (isset($_POST['action'])){
        $action = $_POST['action'];
        if ($action == 'delete-product' && isset($_POST['id'])) {
            delete_product($_POST['id']);
        }
    }
?>